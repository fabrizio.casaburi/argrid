//
//  ViewController.swift
//  ARGrid
//
//  Created by Fabrizio Casaburi on 01/04/2019.
//  Copyright © 2019 ideelibre. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    // MARK: - Outlets
    // ===============
    
    @IBOutlet var sceneView: ARSCNView!
    
    // MARK: - Properties
    // ==================
    
    var gridsCounter = 1
    let gridWidth: CGFloat = 0.6
    let gridRows = 5
    let gridColumns = 5
    
    var previousNearestTile: SCNNode?
    var nearestTile: SCNNode?
    
    // MARK: - View
    // ============
    
    override func viewDidAppear(_ animated: Bool) {
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints,ARSCNDebugOptions.showWorldOrigin]
        
        sceneView.autoenablesDefaultLighting = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleScreenTap))
        sceneView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        configuration.worldAlignment = .gravity
        configuration.planeDetection = [.horizontal]
        configuration.isLightEstimationEnabled = true
        
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }

    // MARK: - Plane detection
    // =======================
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
    
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
    
        drawPlaneNode(on: node, for: planeAnchor)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
    
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        
        node.enumerateChildNodes { (childNode, _) in
            childNode.removeFromParentNode()
        }
        
        drawPlaneNode(on: node, for: planeAnchor)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        // We only want to deal with plane anchors.
        guard let _ = anchor as? ARPlaneAnchor else { return }
        
        // Remove any children this node may have.
        node.enumerateChildNodes { (childNode, _) in
            childNode.removeFromParentNode()
        }
        
    }
    
    func drawPlaneNode(on node: SCNNode, for planeAnchor: ARPlaneAnchor) {
        let plane = SCNNode(geometry: SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z)))
        plane.geometry?.firstMaterial?.isDoubleSided = true
        plane.eulerAngles = SCNVector3(-Double.pi / 2, 0, 0)
        plane.geometry?.firstMaterial?.diffuse.contents = UIColor.white
        plane.position = SCNVector3(planeAnchor.center.x, planeAnchor.center.y-0.1, planeAnchor.center.z)
        plane.opacity = 0.2
        node.addChildNode(plane)
    }
    
    
    // MARK: - User Location
    // =====================
    
    func renderer(_ renderer: SCNSceneRenderer,
                  willRenderScene scene: SCNScene,
                  atTime time: TimeInterval) {
        
        guard let pointOfView = sceneView.pointOfView else { return }
        
        var userPosition = SCNVector3ToGLKVector3(pointOfView.position)
        userPosition.y = 0
        //print("User position: x = \(userPosition.x) y = \(userPosition.y) z = \(userPosition.z)")
        
        var tilesDistances: [SCNNode: Float] = [:]
        
        sceneView.scene.rootNode.enumerateChildNodes { (grid, _) in
            guard let gridName = grid.name, gridName.prefix(4) == "grid" else {
                //print("Grid Detection Failed!")
                return
            }
            //print("\(gridName) detected!")
            grid.enumerateChildNodes { (tile, _) in
                guard let tileName = tile.name, tileName.prefix(4) == "tile" else {
                    //print("Tile Detection Failed!")
                    return
                }
                //print("\(tileName) detected!")
                var tilePosition = SCNVector3ToGLKVector3(tile.worldPosition)
                tilePosition.y = 0
                //print("\(tileName) position: x = \(tilePosition.x) y = \(tilePosition.y) z = \(tilePosition.z)")
                let distance = GLKVector3Distance(userPosition, tilePosition)
                tilesDistances[tile] = distance
            }
        }
        
        self.previousNearestTile = self.nearestTile
        self.nearestTile = tilesDistances.min { a, b in a.value < b.value }?.key
        
        
        let nearestMaterial = SCNMaterial()
        nearestMaterial.diffuse.contents = UIColor.red
        nearestMaterial.name = "nearestMaterial"
        nearestTile?.geometry?.insertMaterial(nearestMaterial, at: 0)
        
        if previousNearestTile?.geometry?.firstMaterial?.name == "nearestMaterial" {
            previousNearestTile?.geometry?.removeMaterial(at: 0)
        }
        
    }
    
    // MARK: - Grid
    // ============
    
    @objc func handleScreenTap(sender: UITapGestureRecognizer) {
        let tappedSceneView = sender.view as! ARSCNView
        let tapLocation = sender.location(in: tappedSceneView)
        
        let planeIntersections = tappedSceneView.hitTest(tapLocation, types: .estimatedHorizontalPlane)
        if !planeIntersections.isEmpty {
            clearGrid()
            drawGrid(for: planeIntersections.first!, of: gridRows, by: gridColumns, wide: gridWidth)
        } else {
            print("Hit Test Failed!")
        }
    }
    
    func drawGrid(for planeIntersection: ARHitTestResult, of rows: Int, by columns: Int, wide width: CGFloat) {
        
        /*guard let currentPointOfView = self.currentPointOfView else {
            return
        }*/
        guard let currentFrame = sceneView.session.currentFrame else {
            return
        }
        
        print("Printing grid #\(gridsCounter)")
        gridsCounter += 1
        
        let planeAnchorTransform = planeIntersection.worldTransform
        let planeAnchorPositionColumn = planeAnchorTransform.columns.3
        
        let grid = SCNNode()
        
        let gridPositionX = Float(CGFloat(currentFrame.camera.transform.columns.3.x))
        let gridPositionY = planeAnchorPositionColumn.y
        let gridPositionZ = Float(CGFloat(currentFrame.camera.transform.columns.3.z)-1.0)
        
        grid.position = SCNVector3(gridPositionX, gridPositionY, gridPositionZ)
        grid.eulerAngles = SCNVector3(Float(-Double.pi / 2), currentFrame.camera.eulerAngles.y, 0)
        
        grid.name = "grid \(gridsCounter)"
        
        sceneView.scene.rootNode.addChildNode(grid)
        
        for row in 1...rows {
            for column in 1...columns {
                //print("Printing tile \(row)x\(column)")
                let tile = SCNNode(geometry: SCNPlane(width: width, height: width))
                let tilePositionX = width*CGFloat(column)-width
                /*let tilePositionY: CGFloat = 0
                let tilePositionZ = width*CGFloat(row)-width*/
                let tilePositionZ: CGFloat = 0
                let tilePositionY = width*CGFloat(row)-width
                
                
                var tileColor: UIColor?
                
                if column % 2 != 0 {
                    if row % 2 != 0 {
                        tileColor = UIColor.black
                    } else {
                        tileColor = UIColor.white
                    }
                } else {
                    if row % 2 != 0 {
                        tileColor = UIColor.white
                    } else {
                        tileColor = UIColor.black
                    }
                }
                
                if column == 1 && row == 1 {
                    tile.geometry?.firstMaterial?.diffuse.contents = UIImage(named: "compass")
                } else {
                    tile.geometry?.firstMaterial?.diffuse.contents = tileColor
                }
                
                tile.position = SCNVector3(tilePositionX, tilePositionY, tilePositionZ)
                
                tile.name = "tile \(row)x\(column)"
                
                grid.addChildNode(tile)
            }
        }
        
    }
    
    func clearGrid () {
        sceneView.scene.rootNode.enumerateChildNodes { (childNode, _) in
            guard let childNodeName = childNode.name, childNodeName.prefix(4) == "grid"
                else { return }
            childNode.removeFromParentNode()
        }
        
        previousNearestTile = nil
        nearestTile = nil
    }

    // MARK: - Session
    // =====================
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}
